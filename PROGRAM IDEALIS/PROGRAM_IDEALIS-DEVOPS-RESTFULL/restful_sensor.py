import RPi.GPIO as GPIO
import time
from time import sleep
import sys
from hx711 import HX711
import MFRC522          
import signal           
import VL53L0X
import os
import configparser
import getpass
import urllib.request



#main program
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(36, GPIO.OUT, initial=GPIO.LOW)#set pin 8 sebagai output dan set nilai inisial = off
GPIO.output(36, GPIO.HIGH)
sleep(0.5)
GPIO.output(36, GPIO.LOW)
sleep(0.5)
GPIO.output(36, GPIO.HIGH)
sleep(0.5)
GPIO.output(36, GPIO.LOW)
sleep(0.5)
GPIO.output(36, GPIO.HIGH)
sleep(0.5)
GPIO.output(36, GPIO.LOW)

GPIO.setwarnings(False)
ada_kartu = True

def end_read(signal,frame):
    global ada_kartu
    ada_kartu = False
    GPIO.cleanup()
    
MIFAREReader = MFRC522.MFRC522()
while ada_kartu :
    signal.signal(signal.SIGINT, end_read)
    (status,TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)
    if status == MIFAREReader.MI_OK:
        print ("PENGGUNA TERDETEKSI")
        print ('')
        (status,uid) = MIFAREReader.MFRC522_Anticoll()
        rfid_id =[uid[0],uid[1],uid[2],uid[3]]
        card_id = str(str(uid[0])+''+str(uid[1])+''+str(uid[2])+''+str(uid[3]))

        print(card_id)
        tof = VL53L0X.VL53L0X()
        tof.start_ranging(VL53L0X.VL53L0X_BEST_ACCURACY_MODE)
        d = tof.get_distance()
        time.sleep(0.3)
        e = tof.get_distance()
        time.sleep(0.3)
        f = tof.get_distance()
        time.sleep(0.3)
        g = tof.get_distance()
        rerata_jarak = ((d + e + f + g)/4)
        jarak = round(rerata_jarak/10)
        time.sleep(1)
        tinggi_paku_gantungan =  187.5 # dalam cm
        tinggi = round(tinggi_paku_gantungan - jarak) # rumus tinggi
        GPIO.cleanup()
        print(tinggi)
        
        # dari HCSR - hitung tinggi
        # dari HX711 - hitung berat
        hx = HX711(5, 6)
        hx.set_reading_format("MSB", "MSB")
        hx.set_reference_unit(2.2)

        hx.reset()
        h = hx.read_long()
        time.sleep(0.3)
        i = hx.read_long()
        time.sleep(0.3)
        j = hx.read_long()
        time.sleep(0.3)
        k = hx.read_long()
        val = ((h + i + j + k) / 4)
        berat = round((val - 270500) / 22000) #rumus berat 270500 = 0, 22000 = naik 1 KG
        hx.power_down()
        hx.power_up()
        print(berat)

        ldr = 0
        
        if berat <= 3:
            print('')
            print('Berat Badan anda tidak terdeteksi')
            GPIO.setwarnings(False)
            GPIO.cleanup()
            GPIO.setmode(GPIO.BOARD)
            GPIO.setup(36, GPIO.OUT, initial=GPIO.LOW)  # set pin 8 sebagai output dan set nilai inisial = off
            GPIO.output(36, GPIO.HIGH)
            sleep(2)
            GPIO.cleanup()
            GPIO.setmode(GPIO.BCM)
            GPIO.setup(24, GPIO.IN)
            if GPIO.input(24) == GPIO.LOW:
                print('')
                print ('Cahaya terdeteksi sinyal error dikirim')
                print ('silahkan menempatkan diri dengan benar ')
                print ('===========================================================')
                print ('Data BMI anda tidak tersimpan ke database perangkat')
                print('')
                ldr = 1
            else:
                print ('error')
                GPIO.cleanup()
                GPIO.setmode(GPIO.BOARD)
                GPIO.setup(36, GPIO.OUT, initial=GPIO.LOW)
        elif tinggi <= 100:
            print('Tinggi Badan anda tidak terdeteksi')
            print('')
            GPIO.setwarnings(False)
            GPIO.cleanup()
            GPIO.setmode(GPIO.BOARD)
            GPIO.setup(36, GPIO.OUT, initial=GPIO.LOW)
            GPIO.output(36, GPIO.HIGH)
            sleep(2)
            GPIO.cleanup()
            GPIO.setmode(GPIO.BCM)
            GPIO.setup(24, GPIO.IN)
            if GPIO.input(24) == GPIO.LOW:
                print('')
                print ('Cahaya terdeteksi sinyal error dikirim')
                print ('silahkan menempatkan diri dengan benar ')
                print ('===========================================================')
                print ('Data BMI anda tidak tersimpan ke database perangkat')
                print('')
                ldr = 1
            else:
                print ('error')
                GPIO.cleanup()
                GPIO.setmode(GPIO.BOARD)
                GPIO.setup(36, GPIO.OUT, initial=GPIO.LOW)
        else:
            ldr = 0

        
        with urllib.request.urlopen('https://idealis.hmte-unsoed.com?access_key=EEOqsRfRR2Q0E5R7CVMZrSNwPCyXP8jj&rfid='+ str(card_id) +'&height=' + str(tinggi) + '&weight=' + str(berat) + '&ldr=' + str(ldr) + '&set_sensor') as response:
            if response.getcode() == 200 :
                html = response.read()
                print(html)
        time.sleep(0.1)
    else:
        pass
#==================================================================================================================================================


